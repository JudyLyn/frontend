let loginForm = document.querySelector("#logInUser")

loginForm.addEventListener("submit" , (e) => {
	e.preventDefault()
	
	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value
	// console.log(email)
	// console.log(password)

	if(email == "" || password == "") {
		alert("please input your email and/or password")
	}
	else {
		fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            // successful authentication will return a JWT via the response accessToken property
            if(data.accessToken){
                //store JWT in local storage
                localStorage.setItem('token', data.accessToken);
                //send a fetch request to decode JWT and obtain user ID and role for storing in context
                fetch(`http://localhost:4000/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    //set the global user state to have properties containing authenticated user's ID and role
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    window.location.replace("./courses.html")
                })
            }else{
            //authentication failure
                alert("something went wrong")
            }

        })

	}




})