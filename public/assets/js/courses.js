let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter;

if(adminUser == "false" || !adminUser) {
    // if user is a regular user, do not show the add course button
	modalButton.innerHTML = null
} else {
    // display add course button if user is an admin
	modalButton.innerHTML = 			
		`			
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
			</div>
		`
}

// fetch the courses from our API

    fetch('http://localhost:4000/api/courses')
    .then(res => res.json())
    .then(data => {
        
        // log the data to check if you were able to fetch the data from our server
        console.log(data)
        let courseData;

        // if the number of courses fetched is less than 1, display no courses available
        if(data.length < 1) {
            courseData = "No courses available"
        }
        else {
            // else iterate the courses collection and display each course
            courseData = data.map(course => {
                
                // check the make up of each element inside the courses collection
                console.log(course._id)
                
                // if the user is a regular user, display when the course was created
                if(adminUser == "false" || !adminUser) 
                    { 
                        cardFooter = `
                            <a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
                        `
                    }
                    else {
                        // check if the user is an admin, display the edit and delete button
                        // when the edit button is clicked, it will redirect the user to editCourse.html
                        // with a query string in the url. 
                        // key = courseId
                        // value  = the element's courseId
                         cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
                                            <a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>
                         `
                     }
                
                // display each element inside the courses collection by accessing its values
                return (
                    `
                    <div class="col-md-6 my-3">
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>${course.name}</h5>
                                <p class='card-text text-left'>
                                    ${course.description}
                                </p>
                                <p class='card-text text-right'>
                                   ₱ ${course.price}
                                </p>

                            </div>
                            <div class='card-footer'>
                                ${cardFooter}
                            </div>
                        </div>
                    </div>
                    `
                )
            // since the collection is an array, we can use the join method to indicate the separator of each
            // element. we replaced the comma(,) with empty strings to remove the commas 
            }).join("")

        }

        let container = document.querySelector("#coursesContainer")

        // get the value of courseData and assign it as the #courseContainer's content
        container.innerHTML = courseData

    })

 







