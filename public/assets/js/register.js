let registerForm = document.querySelector("#registerUser")
// console.log(registerForm)

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

     //validation to enable submit button when all fields are populated and both passwords match
    if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)){

        //check for duplicate email in database first
        fetch('http://localhost:4000/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
        	//if no duplicates found
            if (data === false){
                fetch('http://localhost:4000/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                    	firstName : firstName,
                    	lastName : lastName,
                    	email : email,
                    	password : password1,
                    	mobileNo : mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                	console.log(data)
                    //registration successful
                    if(data === true){
                        alert("registered successfully")
                        //redirect to login
                        window.location.replace("./login.html")
                    }else{
                        //error in creating registration, redirect to error page
                        alert("something went wrong")
                    }
                })
            }
        })
    }else{
        alert("something went wrong")
    }
})