let token = localStorage.getItem("token")
// console.log(token)

let profileContainer = document.querySelector("#profileContainer")



if(!token || token === null) {
	// redirect user to login page
	alert('You must login first')
	window.location.href="./login.html"
} else {

	fetch('http://localhost:4000/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {

		// user details
		// console.log(data)

		let enrollmentData = data.enrollments.map(classData => {
			// console.log(classData)
			return (
						`
							<tr>
								<td>${classData.courseId}</td>
								<td>${classData.enrolledOn}</td>
								<td>${classData.status}</td>
							</tr>	
						`
					)
					
				
		}).join("") //removes commas

			

		profileContainer.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5">		
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="mt-5">Class History</h3>
					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
						</thead>
						<tbody>
							${enrollmentData}
						</tbody>
					</table> 

				</section>
			</div>
		`
	})
}