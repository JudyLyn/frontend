// window.location.search returns the query string part of the URL
// console.log(window.location.search)

// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// spread the values to sey the key-value pairs of the object URLSearchParams
// console.log(...courseId);

// the has method checks if the courseId key exists in the URL query string
// true means that the key exists
// console.log(params.has('courseId'))

// get method returns the value of the key passed in as an argument
// console.log(params.get('courseId'))

let courseId = params.get('courseId')

// retrieve the JWT stored in our local storage for auth
let token = localStorage.getItem('token')

// log the courseId
console.log(courseId)

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://floating-mountain-27941.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>`

	document.querySelector("#enrollButton").addEventListener("click", () => {
    	// insert the course to our courses collection
	    fetch('http://localhost:4000/api/users/enroll', {
	        method: 'POST',
	        headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${token}`
	        },
	        body: JSON.stringify({
	        	courseId : courseId
	        })
	    })
	    .then(res => {
	        return res.json()
	    })
	    .then(data => {
	        //creation of new course successful
	        if(data === true){
	            //redirect to courses page
	            alert('thank you for enrolling! see you!')
	            window.location.replace("./courses.html")
	        }else{
	            //error in creating course
	            alert("something went wrong")
	        }
	    })
	})

})